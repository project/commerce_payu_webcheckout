<?php

namespace Drupal\commerce_payu_webcheckout\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Determines whether user has access to edit a views base entity.
 */
class CheckSignature implements AccessInterface {

  /**
   * An instance of the entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * Builds a new CheckSignature object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * Checks whether signature is correct.
   *
   * AccessResult::neutral() is not used because it is currently the
   * same as setting it to forbidden.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @see https://www.drupal.org/project/drupal/issues/2861074
   */
  public function access(Request $request, AccountInterface $account) {
    $hash_properties = $request->get('extra1');
    if (!$hash_properties) {
      // Do not act if payload in extra 1 is not set.
      return AccessResult::allowed()->setCacheMaxAge(0);
    }

    $hash_properties = unserialize($hash_properties);
    if (!is_array($hash_properties) || !isset($hash_properties['gateway_id'])) {
      // Do not act for gateways other than payu.
      return AccessResult::allowed()->setCacheMaxAge(0);
    }

    // Retrieve the stored hash.
    $hash = $this->entityManager->getStorage('payu_hash')->loadByProperties([
      'commerce_order' => $hash_properties['order_id'],
      'commerce_payment_gateway' => $hash_properties['gateway_id'],
    ]);
    if (!$hash) {
      // Deny access if hash doesn't exist.
      \Drupal::logger('commerce_payu_webcheckout')->notice(
        'Hash inválido: @hash <br /> properties: @properties',
        [
          '@hash' => $hash,
          '@properties' => print_r($hash_properties, TRUE),
        ]
      );
      return AccessResult::forbidden('No PayU Hash found related to this transaction.')->setCacheMaxAge(0);
    }
    $hash = reset($hash);

    // Obtain both the state and signature from request.
    $order = $this->entityManager->getStorage('commerce_order')->load($hash_properties['order_id']);
    $state = $request->get('state_pol');
    $reference_sale = $request->get('reference_sale');
    $signature = $request->get('sign');

    $total_price = $order->getTotalPrice();
    $total_price = (string) number_format($total_price->getNumber(), 2, '.', '');
    if (substr($total_price, -1) === "0") {
      $total_price = substr($total_price, 0, -1);
    }

    // Calculate a new hash and compare it.
    $hash->setComponent('reference_code', $reference_sale);
    $hash->setComponent('amount', $total_price);
    $hash->setComponent('state', $state);
    if ((string) $hash != $signature) {
      \Drupal::logger('commerce_payu_webcheckout')->notice(
        'Hash no coincide con firma: @hash <br />firma: @firma <br />properties: @properties<br />query: @query<br />hashComponents: @hashcomponents',
        [
          '@hash' => $hash,
          '@firma' => $signature,
          '@properties' => print_r($hash_properties, TRUE),
          '@query' => print_r($request->query->all(), TRUE),
          '@hashcomponents' => print_r($hash->getComponents(), TRUE),
        ]
      );
      return AccessResult::forbidden('The provided signature is not valid.')->setCacheMaxAge(0);
    }

    return AccessResult::allowed()->setCacheMaxAge(0);
  }

}
