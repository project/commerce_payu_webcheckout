<?php

namespace Drupal\commerce_payu_webcheckout\Plugin\Commerce\PayuItem;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payu_webcheckout\Plugin\PayuItemBase;

/**
 * Appends algorithm signature method..
 *
 * @PayuItem(
 *   id = "algorithmSignature"
 * )
 */
class AlgorithmSignature extends PayuItemBase {

  /**
   * {@inheritdoc}
   */
  public function issueValue(PaymentInterface $payment) {
    return 'MD5';
  }

}
